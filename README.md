datalife_stock_shipment_out_csv_import
======================================

The stock_shipment_out_csv_import module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_shipment_out_csv_import/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_shipment_out_csv_import)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
